import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.control.Button;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.ImageView;

public class ControladorTriangulo {

    @FXML
    private TextField txtLado2;
    
    @FXML
    private ImageView imageTriangulo;
    
    @FXML
    private TextField txtLado3;

    @FXML
    private Button cmdDeterminar;

    @FXML
    private TextField txtLado1;

    @FXML
    private BorderPane panel1;

    @FXML
    private Label txtTipoTriangulo;

    @FXML
    private Button cmdActualizar;

    @FXML
    private Label lblTitulo;

    @FXML
    private GridPane panelDatos;

    @FXML
    private Button cmdAyuda;

    @FXML
    private Label lblLado1;

    @FXML
    private Label lblLado3;

    @FXML
    private Label lblLado2;

    @FXML
    private Label lblTipoTriangulo;

    @FXML
    private GridPane panelBotones;

    @FXML
    private GridPane panelTipo;
    
    private ModeloTriangulo triangulo;
    
    public ControladorTriangulo(){
    triangulo = new ModeloTriangulo();
    }
    
    @FXML
    void actualizarMedidas() {
    triangulo.setLado1(Float.parseFloat(txtLado1.getText()));
    triangulo.setLado2(Float.parseFloat(txtLado2.getText()));
    triangulo.setLado3(Float.parseFloat(txtLado3.getText()));
    Alert alert = new Alert(Alert.AlertType.INFORMATION, "Medidas actualizadas");
    alert.showAndWait();
    
    }

    @FXML
    void determinarTipo() {
    txtTipoTriangulo.setText(triangulo.getTipo());
    }

    @FXML
    void ayuda() {
    Alert alert = new Alert(Alert.AlertType.INFORMATION, "Determinar el tipo de triángulo según sus lados");
    alert.showAndWait();
    }

}
