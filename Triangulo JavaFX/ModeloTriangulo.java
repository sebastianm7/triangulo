/**
 * Un ejemplo que modela un Triangulo usando POO
 * 
 * @author (Milton JesÃºs Vera Contreras - miltonjesusvc@ufps.edu.co)
 * @version 0.000000000000001 :) --> Math.sin(Math.PI-Double.MIN_VALUE)
 */
public class ModeloTriangulo {

    float lado1;
    float lado2;
    float lado3;

    public ModeloTriangulo() {
    }

    public ModeloTriangulo(float lado1, float lado2, float lado3) {
        this.lado1 = lado1;
        this.lado2 = lado2;
        this.lado3 = lado3;
    }

    public float getLado1() {
        return lado1;
    }

    public void setLado1(float lado1) {
        this.lado1 = lado1;
    }

    public float getLado2() {
        return lado2;
    }

    public void setLado2(float lado2) {
        this.lado2 = lado2;
    }

    public float getLado3() {
        return lado3;
    }

    public void setLado3(float lado3) {
        this.lado3 = lado3;
    }

    
    
    public String getTipo() {
     String tipo = "Desconocido";

     if(lado1==lado2 && lado2==lado3)
      tipo = "Equilatero";
     else {
      if(lado1!=lado2 && lado2!=lado3 && lado1!=lado3)
        tipo = "Escaleno";
      else
        tipo = "Isosceles";
      if(this.esRectangulo())
       tipo = tipo + " Rectángulo";
     }//fin else
     
     return tipo;
    }//fin mÃ©todo getTipo
    
    public boolean esRectangulo(){
        
        float a = this.lado1 * this.lado1;
        float b = this.lado2 * this.lado2;
        float c = this.lado3 * this.lado3;
        
        if (this.lado1 == this.lado2 && c == a + b  || 
                this.lado2 == this.lado3 && a == b + c || 
                this.lado1 == this.lado3 && b == a + c) {
            return true;//complete
        } else if (a > b && a > c && a == b + c || 
                b > a && b > c && b == a + c ||
                c > b && c > a && c == a + b) {
            return true;//complete
        }
        return false;//complete
    

    }//fin mÃ©todo esRectangulo
    

}//fin clase Triangulo

